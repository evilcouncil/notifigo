
FROM	golang:latest	AS	build-stage
WORKDIR	/app
COPY	go.mod	go.sum	./
RUN	go mod download
COPY	*.go	./
RUN	mkdir /dst
RUN	CGO_ENABLED=0 GOOS=linux go build -o /dst/notifigo
FROM	build-stage	AS	run-test-stage
RUN	go test -v ./...
FROM	gcr.io/distroless/base-debian12	AS	build-release-stage
WORKDIR	/
COPY	--from=build-stage /dst/notifigo	/notifigo
USER	nonroot:nonroot
ENV	CONFIG_SERVER	http://config/notifigo.yaml
ENTRYPOINT	["/notifigo"]
