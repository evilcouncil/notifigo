package main

import (
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
)

type AmqTopicConsumer struct {
	Username     string `yaml:"username"`
	Password     string `yaml:"password"`
	Hostname     string `yaml:"server"`
	Port         int    `yaml:"port"`
	RoutingKey   string `yaml:"routing_key"`
	ExchangeName string `yaml:"exchange_name"`
}

func (atc *AmqTopicConsumer) Run(nc chan Notification) {
	log.Println("ATC - Creating connection")
	cs := fmt.Sprintf("amqp://%s:%s@%s:%d", atc.Username, atc.Password, atc.Hostname, atc.Port)

	conn, err := amqp.Dial(cs)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}
	defer ch.Close()

	if err := ch.ExchangeDeclare(atc.ExchangeName, "topic", true, false, false, false, nil); err != nil {
		log.Fatal(err)
	}

	q, err := ch.QueueDeclare(
		"",    // name
		false, //durable
		false, //delete when unused
		true,  // exclusive
		false, //no wait
		nil,   //args
	)
	if err != nil {
		log.Fatal(err)
	}

	if err := ch.QueueBind(q.Name, atc.RoutingKey, atc.ExchangeName, false, nil); err != nil {
		log.Fatal(err)
	}

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Fatal(err)
	}

	for msg := range msgs {
		log.Printf("Routing key: %s", msg.RoutingKey)
		log.Printf("Got message: %s", msg.Body)
		n := Notification{}
		if err := json.Unmarshal(msg.Body, &n); err != nil {
			log.Printf("Error while unmarshalling message: %s", err)
			continue
		}
		n.NotificationChannel = msg.RoutingKey
		log.Printf("Sending %v", n)
		nc <- n
	}

}

func NewAmqTopicConsumer(username string, password string, hostname string, port int, routingkey string) (*AmqTopicConsumer, error) {
	atc := new(AmqTopicConsumer)
	atc.Hostname = hostname
	atc.Password = password
	atc.Port = port
	atc.Username = username
	atc.RoutingKey = routingkey

	return atc, nil
}
