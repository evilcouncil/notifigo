package main

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	//"time"
	//external
)

type Notification struct {
	NotificationChannel string `json:"notification_channel"`
	Message             string `json:"Message"`
}

type Publisher interface {
	Run(nc chan Notification)
}

type NotificationConsumer interface {
	Run(nc chan Notification)
	AddChannel(channel string, url string) error
}

func main() {
	log.Println("Main - Load .env file")
	if err := godotenv.Load("notifigo.env"); err != nil {
		log.Printf("Unable to load env file - %s", err)
	}
	configServer := os.Getenv("CONFIG_SERVER")

	log.Println("Main - downloading configs")

	config, err := GetConfig(configServer)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Main - creating channel")
	notifications := make(chan Notification, 3)
	log.Println("Main - creating amq consumer")

	log.Println("Main - creating publisher")
	dp := NewDiscordPublisher()
	for _, nc := range config.Channels {
		if err := dp.AddChannel(nc); err != nil {
			log.Panic(err)
		}

	}

	log.Println("Main - starting publisher")
	go dp.Run(notifications)
	log.Printf("Listening for notification from AMQ")

	config.AmqTopicConsumer.Run(notifications)

	close(notifications)

}
