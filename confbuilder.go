package main

import (
	"gopkg.in/yaml.v2"
	"io"
	"log"
	"net/http"
)

type NotificationChannel struct {
	RoutingKey string `yaml:"routing_key"`
	Webhook    string `yaml:"webhook"`
}

type Config struct {
	/*
		RabbitInfo struct {
			Username   string `yaml:"username"`
			Password   string `yaml:"password"`
			Server     string `yaml:"server"`
			Port       int    `yaml:"port"`
			RoutingKey string `yaml:"routing_key"`
		} `yaml:"rabbit_info"`
	*/
	AmqTopicConsumer `yaml:"rabbit_info"`
	Channels         []NotificationChannel `yaml:"channels"`
}

func GetConfig(target string) (Config, error) {
	log.Printf("Loading configs from %s", target)
	resp, err := http.Get(target)
	if err != nil {
		return Config{}, err
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return Config{}, err
	}
	log.Printf("Parsing configs")
	config := Config{}
	if err := yaml.Unmarshal(data, &config); err != nil {
		return Config{}, err
	}

	return config, nil
}
