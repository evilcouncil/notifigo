package main

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestGetConfig(t *testing.T) {
	expected := Config{}
	expected.AmqTopicConsumer.Username = "testuser"
	expected.AmqTopicConsumer.Password = "password"
	expected.AmqTopicConsumer.Hostname = "rabbit.example.com"
	expected.AmqTopicConsumer.Port = 5672
	expected.AmqTopicConsumer.RoutingKey = "routing.key"
	expected.AmqTopicConsumer.ExchangeName = "testexchange"
	expected.Channels = append(expected.Channels, NotificationChannel{RoutingKey: "routing.key.test", Webhook: "http://webhook.example.com"})

	expectedBytes, err := yaml.Marshal(expected)
	if err != nil {
		t.Error(err)
	}

	server := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			if _, err := w.Write(expectedBytes); err != nil {
				t.Error(err)
			}
		},
	))
	defer server.Close()

	config, err := GetConfig(server.URL)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(expected, config) {
		msg := fmt.Sprintf("expected: %v, actual %v", expected, config)
		t.Error(msg)
	}
}
