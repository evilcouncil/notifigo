module gitlab.com/evilcouncil/notifigo

go 1.21

toolchain go1.22.1

require (
	github.com/disgoorg/disgo v0.17.2
	github.com/hashicorp/go-envparse v0.1.0
	github.com/rabbitmq/amqp091-go v1.9.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/disgoorg/json v1.1.0 // indirect
	github.com/disgoorg/snowflake/v2 v2.0.1 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/sasha-s/go-csync v0.0.0-20240107134140-fcbab37b09ad // indirect
)
