package main

import (
	"github.com/disgoorg/disgo/webhook"
	"log"
)

type DiscordPublisher struct {
	Channels map[string]webhook.Client
}

func (dp *DiscordPublisher) Run(nc chan Notification) {
	log.Println("DP - Waiting for messages")
	for n := range nc {
		if _, err := dp.Channels[n.NotificationChannel].CreateContent(n.Message); err != nil {
			panic(err)
		}
	}
}

func (dp *DiscordPublisher) AddChannel(nc NotificationChannel) error {
	log.Printf("Adding channel: %s url: %s", nc.RoutingKey, nc.Webhook)
	client, err := webhook.NewWithURL(nc.Webhook)
	if err != nil {
		return err
	}
	dp.Channels[nc.RoutingKey] = client
	return nil
}

func NewDiscordPublisher() *DiscordPublisher {
	dp := new(DiscordPublisher)
	dp.Channels = make(map[string]webhook.Client)
	return dp
}
